var GoogleSearch = require('google-search');
var Crawler = require("crawler");
var cheerio = require('cheerio');

var googleSearch = new GoogleSearch({
    key: 'AIzaSyAN0uO5stRlf2oujhELHnk6y2NwroaXyl4',
    cx: '001994601603263861534:j85pauiisxs'
});

customSearch = function(restName, googlePage, lang) {

        var hl = ["lang_zh-tw", "lang_zh-cn", "lang_en"]
        var openriceLangURL = "/zh/hongkong/r-"
        if (lang === "en_US"){
          hl = ["lang_en"];
          openriceLangURL = "/en/hongkong/r-"
        } else if (lang === "zj_HK"){
          hl = ["lang_zh-tw"]
        }

        var query = restName;
        var googleResult = [];
        var arrResult = [];
        var pricerange ="";

            return new Promise((resolve, reject) => {
              googleSearch.build({
                // q: query + "-的相片 -的餐牌 -的食評 -已結業 -已搬遷",
                q: query,
                // excludeTerms: "closed",
                cr: "hk",
                start: googlePage,
                // fileType: "",
                gl: "hk", //geolocation,
                // lr: ["lang_zh-tw", "lang_zh-cn", "lang_en"],
                // lr: ["lang_en"], //should ignore
                hl: hl,
                num: 10, // Number of search results to return between 1 and 10, inclusive
                siteSearch: "http://openrice.com/" // Restricts results to URLs from a specified site
            }, function (error, response) {

                if (response.items){
                  console.log(response.items);

                  var links = [];

                  response.items.forEach(function(data){
                    if (data.link)
                    if (data.link.includes(openriceLangURL))
                    if ((!data.link.includes("?what=")) && (!data.link.includes("/directory/"))
                      &&(!data.link.includes("/review/")) && (!data.link.includes("?where="))
                      && (!data.link.includes("/job/")) && (!data.link.includes("/reviews"))
                      && (!data.link.includes("/photos")) && (!data.link.includes("/menus"))
                      && (!data.link.includes("/notices")) && (!data.link.includes("/buffet"))
                      && (!data.link.includes("/videos")) && (!data.link.includes("/jobs"))
                      && (!data.link.includes("/offer/")) && (!data.link.includes("/p-")) ) links.push(data.link);

                  });

                  resolve(links);
                } else {
                  var links = [];
                  resolve(links);
                }


            });
          });
        };



//Testing
// var exports={};
// var req = {
//   "name":"MX"
// }

exports.index = (request, response) => {
var i = 1;
var name = request.query.name;

console.log("function start-------")
console.log("name: ", name);

async function runCustomSearch(name, i){

  var result = [];
  console.log(name)
  result = await customSearch(name, i, request.query.lang)
  console.log(result);
  console.log(result.length);

  return result;

}

var crawlResult = {
  "error":"",
  "success": "false",
  "data":""
}

runCustomSearch(name, i, response)
.then(result => {
  console.log(result);
  
  if (result.length){
    crawlResult.googleResult = result;
    crawlResult.googleResultCount = result.length;
    crawlResult.url = result[0];
    c.queue(result[0])
  } else {
    crawlResult.error = "Google Returns no result";
    console.log(crawlResult);
    response.status(200).send(crawlResult);
    //end
  }
})
.catch(err => {console.error(err);}) //return error



  var c = new Crawler({
      maxConnections : 5,
      retries: 10,
      // skipEventRequest: false, // default to true, direct requests won't trigger Event:'request'
      callback : function (error, res, done) {
          if(error) {
              console.log(error);
              crawlResult.error = error;
              crawlResult.success = "true";
              return crawlResult;
          } else {
              console.log(res.statusCode);
              var $ = res.$;
              // console.log($.html())
              // console.log($("title").text())

              var restdata = {};

              //Name
              console.log($('.poi-name-container .name').text());
              restdata.name = $('.poi-name-container .name').text().replace(/(\r\n|\n|\r)/gm, "").trim();
              console.log($('.poi-name-container .smaller-font-name').text())
              restdata.altName= $('.poi-name-container .smaller-font-name').text().replace(/(\r\n|\n|\r)/gm, "").trim();

              //Address
              var address = $('.address-section .content').first().text().replace(/(\r\n|\n|\r)/gm, "").trim();
              console.log(address)
              restdata.address = address;

              //Phone
              console.log($('.telephone-section .content').first().text().replace(/(\r\n|\n|\r)/gm, "").trim())
              restdata.phone = $('.telephone-section .content').first().text().replace(/(\r\n|\n|\r)/gm, "").trim();

              //Good for (Occassions)
              console.log($('.good-for-section .content').first().text().replace(/(\r\n|\n|\r)/gm, "").trim())
              restdata.occassions = $('.good-for-section .content').first().text().replace(/(\r\n|\n|\r)/gm, "").trim();

              //Opening Hours
              restdata.openingHours = [];
              $('.opening-hours-list .opening-hours-date').first().remove()
              $('.opening-hours-list .opening-hours-date').each(function(i,elem){
                console.log($(this).text().replace(/(\r\n|\n|\r)/gm, "").trim());
                restdata.openingHours.push($(this).text().replace(/(\r\n|\n|\r)/gm, "").trim());
              })

              restdata.openingHoursTime = [];
              $('.opening-hours-list .opening-hours-time div').first().remove()
              $('.opening-hours-list .opening-hours-time div').each(function(i,elem){
                console.log($(this).text().replace(/(\r\n|\n|\r)/gm, "").trim());
                restdata.openingHoursTime.push($(this).text().replace(/(\r\n|\n|\r)/gm, "").trim());
              })

              //District
              console.log($('.header-poi-district').first().text().replace(/(\r\n|\n|\r)/gm, "").trim())
              restdata.district = $('.header-poi-district').first().text().replace(/(\r\n|\n|\r)/gm, "").trim();

              //PriceRange
              console.log($('.header-poi-price').first().text().replace(/(\r\n|\n|\r)/gm, "").trim())
              restdata.PriceRange = $('.header-poi-price').first().text().replace(/(\r\n|\n|\r)/gm, "").trim();;

              //Tags
              restdata.tags = [];
              $('.header-poi-categories a').each(function(i,elem){
                console.log($(this).text().replace(/(\r\n|\n|\r)/gm, "").trim());
                restdata.tags.push($(this).text().replace(/(\r\n|\n|\r)/gm, "").trim());
              })

              // console.log($('.header-poi-categories a').text().replace(/(\r\n|\n|\r)/gm, "").trim())
              // restdata.tags = $('.header-poi-categories a').text().replace(/(\r\n|\n|\r)/gm, "").trim();

              //Others
              // restdata.others = [];
              //  $('.conditions-section').each(function(i,elem){
              //    console.log($(this).text().replace(/(\r\n|\n|\r)/gm, "").trim());
              //    restdata.others.push($(this).text().replace(/(\r\n|\n|\r)/gm, "").trim());
              //  })


              crawlResult.data = restdata;
              crawlResult.success = true;

              console.log(crawlResult);
              response.status(200).send(crawlResult);

          }

          done();
      }
  });
}

// Testing
// exports.index(req,"");
