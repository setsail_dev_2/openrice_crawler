var Crawler = require("crawler");
var cheerio = require('cheerio');

exports.crawl = function (url){

  var result = {
    "error":"",
    "success":false,
    "data":""
  }

  var c = new Crawler({
      maxConnections : 5,
      retries: 10,
      // skipEventRequest: false, // default to true, direct requests won't trigger Event:'request'
      callback : function (error, res, done) {
          if(error) {
              console.log(error);
              result.error = error;
              result.success = true;
              return result;
          } else {
              console.log(res.statusCode);
              var $ = res.$;
              // console.log($.html())
              // console.log($("title").text())

              var restdata = {};

              //Name
              console.log($('.poi-name-container .name').text());
              restdata.name = $('.poi-name-container .name').text();
              console.log($('.poi-name-container .smaller-font-name').text())
              restdata.altName= $('.poi-name-container .smaller-font-name').text();

              //Address
              console.log($('.address-section .content').first().text())
              restdata.address = $('.address-section .content').first().text();

              //Phone
              console.log($('.telephone-section .content').first().text())
              restdata.phone = $('.telephone-section .content').first().text();

              //Opening Hours
              restdata.openingHours = [];
              $('.opening-hours-list .opening-hours-date').first().remove()
              $('.opening-hours-list .opening-hours-date').each(function(i,elem){
                console.log($(this).text());
                restdata.openingHours.push($(this).text());
              })
              restdata.openingHoursTime = [];
              $('.opening-hours-list .opening-hours-time').each(function(i,elem){
                console.log($(this).text());
                restdata.openingHoursTime.push($(this).text());
              })

              //District
              console.log($('.header-poi-district').first().text())
              restdata.district = $('.header-poi-district').first().text()

              //PriceRange
              console.log($('.header-poi-price').first().text())
              restdata.PriceRange = $('.header-poi-price').first().text();

              //Tags
              console.log($('.header-poi-categories').first().text())
              restdata.tags = $('.header-poi-categories').first().text()

              //Others
              restdata.others = [];
               $('.conditions-section').each(function(i,elem){
                 console.log($(this).text());
                 restdata.others.push($(this).text());
               })


              result.data = restdata;
              result.success = true;

              console.log(result);


          }

           done();
      }
  });

  c.queue(url);
}
//
// var c = new Crawler({
//     maxConnections : 5,
//     retries: 10,
//     // This will be called for each crawled page
//     callback : function (error, res, done) {
//         if(error){
//             console.log(error);
//
//         }else{
//             var $ = res.$;
//             // console.log($.html())
//             // console.log($("title").text())
//
//             //Name
//             console.log($('.poi-name-container .name').text())
//             console.log($('.poi-name-container .smaller-font-name').text())
//
//             //Address
//             console.log($('.address-section .content').first().text())
//             //Phone
//             console.log($('.telephone-section .content').first().text())
//             //Opening Hours
//             $('.opening-hours-list .opening-hours-date').first().remove()
//             $('.opening-hours-list .opening-hours-date').each(function(i,elem){console.log($(this).text())})
//             $('.opening-hours-list .opening-hours-time').each(function(i,elem){console.log($(this).text())})
//
//             //District
//             console.log($('.header-poi-district').first().text())
//             //PriceRange
//             console.log($('.header-poi-price').first().text())
//             //Tags
//             console.log($('.header-poi-categories').first().text())
//
//             //Others
//              $('.conditions-section').each(function(i,elem){console.log($(this).text())})
//
//
//         }
//         done();
//
//     }
// });

// c.queue('https://www.openrice.com/zh/hongkong/r-%E6%98%9F%E5%B7%B4%E5%85%8B%E5%92%96%E5%95%A1-%E6%97%BA%E8%A7%92-%E7%BE%8E%E5%9C%8B%E8%8F%9C-%E6%B2%99%E5%BE%8B-r12884');
